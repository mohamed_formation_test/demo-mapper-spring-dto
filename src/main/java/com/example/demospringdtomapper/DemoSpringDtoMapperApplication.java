package com.example.demospringdtomapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringDtoMapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringDtoMapperApplication.class, args);
	}

}
