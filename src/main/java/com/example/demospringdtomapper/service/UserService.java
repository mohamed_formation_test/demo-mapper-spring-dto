package com.example.demospringdtomapper.service;

import com.example.demospringdtomapper.dto.UserDto;

public interface UserService {

    public UserDto createUser(UserDto userDto);

    public UserDto readUser(Integer id);

    public UserDto updateUser(UserDto userDto);



}
