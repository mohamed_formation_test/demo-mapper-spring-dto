package com.example.demospringdtomapper.service.impl;

import com.example.demospringdtomapper.dto.UserCreateDto;
import com.example.demospringdtomapper.dto.UserDto;
import com.example.demospringdtomapper.dto.UserReadDto;
import com.example.demospringdtomapper.dto.UserUpdateDto;
import com.example.demospringdtomapper.entity.User;
import com.example.demospringdtomapper.repository.UserRepository;
import com.example.demospringdtomapper.service.UserService;
import com.example.demospringdtomapper.utils.DtoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DtoUtils dtoUtils;


    @Override
    public UserDto createUser(UserDto userDto) {

        User user = dtoUtils.convertToEntity(new User(), userDto);

       User user1 = userRepository.save(user);

        return dtoUtils.convertToDto(user1,new UserCreateDto());
    }

    @Override
    public UserDto readUser(Integer id) {
        return dtoUtils.convertToDto(userRepository.findById(id).get(),new UserReadDto());
    }

    @Override
    public UserDto updateUser(UserDto userDto) {

        User user = dtoUtils.convertToEntity(new User(), userDto);

        User user2 = userRepository.save(user);

        return dtoUtils.convertToDto(user2, new UserUpdateDto()) ;
    }
}
