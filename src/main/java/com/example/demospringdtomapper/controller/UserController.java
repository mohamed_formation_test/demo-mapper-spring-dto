package com.example.demospringdtomapper.controller;


import com.example.demospringdtomapper.dto.UserCreateDto;
import com.example.demospringdtomapper.dto.UserDto;
import com.example.demospringdtomapper.dto.UserUpdateDto;
import com.example.demospringdtomapper.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1")
public class UserController {


        @Autowired
        UserServiceImpl userServiceImpl;

        @PostMapping("/create-user")
        public ResponseEntity<UserDto> createUser(@RequestBody UserCreateDto userDto){
            return new ResponseEntity<>(userServiceImpl.createUser(userDto), HttpStatus.CREATED);
        }

        @GetMapping("/get-user/{id}")
        public ResponseEntity<UserDto> getUserById(@PathVariable Integer id){
            return new ResponseEntity<>(userServiceImpl.readUser(id),HttpStatus.OK);
        }

        @PutMapping("/update-user/{id}")
        public ResponseEntity<UserDto> updateUser(@PathVariable Integer id,@RequestBody UserUpdateDto userDto){
            userDto.setId(id);
            return new ResponseEntity<>(userServiceImpl.updateUser(userDto),HttpStatus.ACCEPTED);
        }

    }
